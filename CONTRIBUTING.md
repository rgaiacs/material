Jovem Hacker é um projeto livre
e recebemos contribuições de todas as formas:
novas lições,
correções no material existente,
notificação de erro
e revisão em propostas de alterações.

## Licenciamento

Ao contribuir,
você licencia seu trabalho sobre a licença CC-BY-SA,
a mesma utilizada pelo Jovem Hacker.

## Regras gerais

#.  O gerenciamento de contribuições
    ocorre em <http://gitlab.com/jovemhacker/material/>.

#.  Para reportar um problema
    ou sugerir uma melhoria
    utilize <https://gitlab.com/jovemhacker/material/issues/new>.

#.  Caso você deseje enviar uma correção ou melhoria
    que você já realizou,
    sugerimos que você fork <http://gitlab.com/jovemhacker/material/>
    e envie um "merge request".

#.  Se você está procurando formas para contribuir
    verifique <https://gitlab.com/jovemhacker/material/issues>.

## Arquivos e diretórios:

-   `CONTRIBUTING.md`

    Armazena as sugestões de contribuição.

-   `dist/`

    É a raiz para a versão compilada em HTML das lições.

-   `gruntfile.js`

    Armazena as regras de compilação das lições para HTML.

-   `index.md`

    É a página inicial das lições.
    Contem links para todas as lições.

-   `LICENSE.md`

    A licença utilizada para as lições. CC-BY-SA.

-   `package.json`

    As dependências do projeto.

-   `README.md`

    Arquivo com explicação breve sobre o projeto.

-   `src/`

    É a raiz para as lições.

-   `src/*.md`

    São os arquivos Markdown que apresentam o material.

-   `src/css/`

    Armazena as folhas de estilo utilizadas nas lições.

-   `src/hardware/`

    É a raiz para o material de Hardware.

-   `src/hardware/[0-9][0-9]-*.md`

    São os arquivos Markdown do material de Hardware.

-   `src/img/`

    Armazena imagens utilizadas globalmente.

-   `src/img/hardware/`

    Armazena imagens utilizadas no material de Hardware.

-   `src/img/python`

    Armazena imagens utilizadas no material de Python.

-   `src/img/scratch`

    Armazena imagens utilizadas no material de Scratch.

-   `src/img/web`

    Armazena imagens utilizadas no material de Web.

-   `src/instrutores-*/`

    É a raiz para a notas aos instrutores.

-   `src/instrutores-*/exemplos/`

    É a raiz para os exemplos.

-   `src/python/`

    É a raiz para o material de Python.

-   `src/python/[0-9][0-9]-*.md`

    São os arquivos Markdown do material de Python.

-   `src/scratch/`

    É a raiz para o material de Scratch.

-   `src/scratch/[0-9][0-9]-*.md`

    São os arquivos Markdown do material de Scratch.

-   `src/web/`

    É a raiz para o material sobre Web.

-   `src/web/[0-9][0-9]-*.md`

    São os arquivos Markdown do material de Web.

-   `templates/`

    Armazena os templates utilizados pelo Pandoc.

## Regras de escrita

#.  Utilizamos a terceira pessoa do singular.
    E.g.

    > Selecione o bloco ...

    **ao invés de**

    > Selecionamos o bloco ...

#.  Utilizamos elementos `<code>`
    para indicar elementos da palheta de blocos do Scratch.
    E.g.

    > Na palheta `Movimento` você encontra ...

#.  Utilizamos parênteses, ( e ), para delimitar
    valores customizáveis nos blocos do Scratch.
    Se estiver indicando qual o bloco
    não utilize nenhum valor entre os parênteses.
    Se estiver representando a aba `Comandos`
    preencha o valor entre parênteses.
    E.g.

    > Utilize o bloco `vá para x: () y: ()`

    e

    > Seu código deve ficar como
    >
    > ~~~
    > quando ⚑ clicado
    > vá para x: (0) y: (0)
    > ~~~

## Guia de estilo para Markdown

Utilizamos [Pandoc](http://pandoc.org/) como ferramenta de conversão
de Markdown para HTML (e futuramente EPUB e PDF).
Pedimos que siga as seguintes regras.

#.  Todo arquivo deve começar com um bloco YAML
    contendo os metadados correspodentes.

#.  Linhas não devem ter mais de 80 caracteres
    **exceto** quando for um link externo.

#.  Títulos de (sub)seções devem utilizar o padrão Atx.
    E.g.

    ~~~
    # Título da seção
    ~~~

#.  Blocos de código devem ser ser delimitados por `~~~`
    e ter identificado a linguagem do código em questão.
    E.g. um código em Python deve ser escrito como

    ~~~~~~
    ~~~{.python}
    print("Olá")
    ~~~
    ~~~~~~

#.  Listas não numeradas devem utilizar `-`
    e listas numeradas devem utilizar `#.`

#.  Tabelas devem ser construidas utilizando `grid_tables`.

#.  É permitido utilizar `<div>` para criação de caixas
    desde que atribuido uma **e apenas uma** das seguintes classes:

    -   exercicio.

## Guia de estilo para screenshot

Ao fazer uso de um screenshot
ele deve seguir as seguintes regras:

#.  O screenshot shot deve ser reprodutível.
    e os arquivos necessários para isso devem estar armazenados em
    `src/instrutores-*/exemplos/`.

#.  O programa esteja em português (Brasil).

#.  Armazenados em `src/img`.

#.  Utilizado o formato JPG.

#.  Nomeado utilizando apenas letras minúsculas e `-`.

#.  A resolução da tela deve ser 800x600
    e a imagem gerada também deve ter 800x600 pixels.

#.  A janela desejada deve estar no modo "full-screen".

#.  Para destacar algo,
    selecionar uma região retangular
    e contornar a região com uma linha de 20px na cor `#82ed00`.

#.  Para aumentar algo,
    selecionar uma região retangular,
    contornar a região com uma linha de 14px na cor `#ffffff`,
    ampliar a região em 140%
    e ajustar a posição.
