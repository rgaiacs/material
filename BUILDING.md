O material está escrito em Markdown
e você precisa convertê-lo para HTML
se desejar utilizá-lo através de um navegador web.

Aqui você vai encontrar instruções
de como converter o material.

# Configuração

## GNU/Linux

Iremos tomar como base a distribuição de GNU/Linux Debian.
Se você utilizar outra distribuição de GNU/Linux
acreditamos que você saberá adaptar as instruções corretamente.

#.  Instale [Pandoc]:

    ~~~
    # apt-get install pandoc
    ~~~

#.  Instale [Node]:

    ~~~
    # apt-get install node
    ~~~

#.  Instalar o [Grunt]:

    ~~~
    # npm install grunt-cli -g
    ~~~

#.  Instale as dependências locais:

    ~~~
    $ npm install
    ~~~

## Mac OS X/Windows

#.  Instale [Pandoc], [Node] e [Grunt]
    utilizando o instalador disponibilizado
    no respectivos sites.

#.  Instale as dependências locais:

    ~~~
    $ npm install
    ~~~

# Conversão

Utilize

~~~
$ grunt serve
~~~

O material estará acessível em <http://localhost:8100>.

[Grunt]: (http://gruntjs.com/)
[Node]: http://npmjs.org/
[Pandoc]: http://pandoc.org/
