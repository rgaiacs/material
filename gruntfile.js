/* jshint node: true */
module.exports = function(grunt) {
  'use strict';
  var path = require('path');

  grunt.loadNpmTasks('grunt-panda');
  grunt.loadNpmTasks('grunt-express');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jsbeautifier');
  grunt.loadNpmTasks('grunt-mdlint');

  grunt.initConfig({
    express: {
      web: {
        options: {
          port: '<%= pkg.config.port %>',
          server: path.resolve(__dirname, 'scripts/server.js'),
        },
      },
    },
    build: {
      web: {
        options: {
          process: true,
          pandocOptions: ['--from=markdown',
            '--to=html5',
            '--smart',
            '--template=./templates/jovemhacker.html5',
          ],
        },
        files: [{
          expand: true,
          cwd: 'src',
          src: '**/*.md',
          dest: 'dist/web',
          ext: '.html',
        }],
      },
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc',
      },
      gruntjs: {
        src: ['<%= meta.files.gruntfile %>'],
      },
      scripts: {
        scr: ['<%= meta.files.scripts %>'],
      },
    },
    jsbeautifier: {
      check: {
        src: [
          '<%= meta.files.gruntfile %>',
          '<%= meta.files.scripts %>',
          '<%= meta.files.css %>',
        ],
        options: {
          config: '.jsbeautifyrc',
          mode: 'VERIFY_ONLY',
        },
      },
      modify: {
        src: [
          '<%= meta.files.gruntfile %>',
          '<%= meta.files.scripts %>',
          '<%= meta.files.css %>',
        ],
        options: {
          config: '.jsbeautifyrc',
        },
      },
    },
    mdlint: {
      markdownsrc: ['<%= meta.files.markdownsrc %>'],
      docs: ['<%= meta.files.docs %>'],
    },
    meta: {
      files: {
        docs: '*.md',
        gruntfile: 'gruntfile.js',
        scripts: 'scripts/**/*.js',
        markdownsrc: 'src/**/*.md',
        css: 'src/css/**/*.css',
      },
    },
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      css: {
        files: ['<%= meta.files.css %>'],
        tasks: ['jsbeautifier:check'],
      },
      docs: {
        files: ['!<%= meta.files.docs %>'],
        tasks: ['mdlint'],
      },
      markdownsrc: {
        files: ['<%= meta.files.markdownsrc %>'],
        tasks: ['build:web', 'mdlint'],
      },
      gruntjs: {
        files: ['<%= meta.files.gruntfile %>'],
        tasks: ['jshint:gruntjs', 'jsbeautifyrc:check'],
      },
      scripts: {
        files: ['<%= meta.files.scripts %>'],
        tasks: ['jshint:scripts', 'jsbeautifyrc:check'],
      },
    },
  });

  grunt.registerTask('serve', ['express:web', 'express-keepalive']);

  grunt.renameTask('panda', 'build');

  grunt.registerTask('default', ['build:web', 'watch']);
};
