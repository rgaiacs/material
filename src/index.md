O objetivo do Projeto Jovem Hacker é auxiliar na formação de uma geração que
seja autônoma tecnologicamente e esteja melhor preparada para definir os rumos
do desenvolvimento tecnológico na sociedade. No projeto buscamos compartilhar e
construir conhecimento sobre cultura digital, software livre, recursos e dados
abertos, e o trabalho colaborativo.

O empoderamento tecnológico vai de encontro a noção de e "usuários", dependentes
de um sistema tecnológico cada vez mais complexo, fechado e ubíquo, sem entender
seu funcionamento, nem tendo autonomia para interferir em seu funcionamento.
Focamos em incentivar esses jovens, particularmente mulheres, para que possam
incorporar novas ferramentas em áreas de atuação já consolidadas (como o
jornalismo, educação, ou desenvolvimento de sistemas).

A formação do Projeto Jovem Hacker está distribuída em módulos, e tem seu foco
no aprendizado através de projetos práticos e colaborativos. Tomamos como base a
noção de end-user programming, focando a formação em atividades relacionadas ao
remix, mashup e reuso. Não buscamos formar exímios programadores, mas tinkerers,
ou "fuçadores" que possam entender os princípios dos códigos de computadores,
customizar, modificar e criar em cima de códigos e programas existentes.
Acreditamos que jovens podem se sentir motivados a entender o quê acontece com
os programas que usam, particularmente quando desenvolvem a capacidade de
modificá-los. O Projeto Jovem Hacker, é portanto, um experimento em formação.
Adotamos alguns preceitos básicos, focados em uma ética hacker: o uso de software livre,
o compartilhamento livre de código, e o trabalho colaborativo, aberto.

O Projeto Jovem Hacker é uma iniciativa do [NIED](http://nied.unicamp.br/) ([Unicamp](http://unicamp.br/))
e do [Coletivo Revoada](http://revoada.net.br/).
Em 2015, o projeto está sendo conduzido em duas cidades (Campinas e Capivari)
com experimentações e currículos distintos, mas com planejamento conjunto.

## Sumário

#.  [Bem-vindo!!!](/bem-vindo.html)

#.  Hardware

    #.  [Componentes do hardware](hardware/01-componentes.html)
    #.  [Comunicação entre componentes](hardware/02-comunicacao.html)
    #.  [Ligando um LED](hardware/03-led.html)

#.  Scratch

    #.  [Interface](/scratch/01-interface.html)
    #.  [Movimento](/scratch/02-movimento.html)
    #.  [Paralelismo](/scratch/03-paralelismo.html)
    #.  [Evento](/scratch/04-evento.html)
    #.  [Laço](/scratch/05-laco.html)
    #.  [Condicional](/scratch/06-condicional.html)
    #.  [Operador]()
    #.  [Variável]()

#.  Web

#.  Python
