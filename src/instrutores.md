---
title: Jovem Hacker - Notas aos instrutores
---

Essas notas foram escritas
objetivando auxiliar os instrutores
a utilizarem o [materia escrito](/index.html).

Cada uma das notas encontra-se dividida em quatro seções:

-   Exemplos

    Possui links para os exemplos utilizados no material escrito
    e para outros exemplos que podem ser utilizados
    para motivar os alunos.

-   Computação desplugada

    Possui sugestão de atividades para serem feitas longe dos computadores.

-   Comentários

    Possui comentários sobre o material escrito.

-   Exercícios adicionais

    Possui sugestões de exercícios adicionais.

## Sumário

#.  Hardware

#.  Scratch

    #.  [Interface](/instrutores-scratch/01-interface.html)
    #.  [Movimento]()
    #.  [Paralelismo](/instrutores-scratch/03-paralelismo.html)
    #.  [Eventos](/instrutores-scratch/04-evento.html)
    #.  [Laços]()
    #.  [Condicionais](/instrutores-scratch/06-condicional.html)
    #.  [Operadores]()
    #.  [Variáveis]()

#. Web

#. Python
