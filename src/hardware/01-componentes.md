---
track: Hardware
title: Componentes do hardware
---

Um computador ou dispositivo eletrônico semelhante,
como celular e tablet,
possui **obrigatoriamente** os seguintes componentes:

-   0s e 1s
-   processador,
-   memória,
-   dispositivo de armazenamento,
-   dispositivo de entrada, e
-   dispositivo de saída.

### 0s e 1s

Em computadores, todos os dados são representados por 0s e 1s (também chamados de Sim/Não, Verdadeiro/Falso ou Ligado/Desligado).

Podemos representar qualquer número apenas com 0s e 1s, através da representação binária.
Da mesma forma que em decimal cada digito vale 10X mais que o digito vizinho, em binário cada digito vale 2X o valor do digito vizinho.

Por exemplo, para o número 106:

-   Em decimal, 106 = 1×100 + 0×10 + 6×1
-   Em binário, 1101010 = 1×64 + 1×32 + 0×16 + 1×8 + 0×4 + 1×2 + 0×1

Em binário, são necessários mais digitos representar o mesmo número (~3X)

Computadores sempre fazem contas com um número fixo de digitos binários. Por exemplo, um computador de 64 bits sempre representa números com 64 digitos binários.

#### Bits, Bytes, Kilobytes, Megabytes, Gigabytes, Terabytes

TODO

### Processador

Dizem ser o cérebro do computador, mas na verdade é bem burrinho ;)

O que o processador faz é seguir instruções simples, passo-a-passo.

De forma geral, o processador faz apenas 3 tipos de operação, todas muito simples:

-   Aritmeticas (Soma, multiplicação, comparação, etc)
-   Pular para outro passo dependendo do resultado da comparação
-   Ler e gravar dados na memória RAM

Por exemplo, para somar todos os numeros de 1 até 10 e armazenar o resultado na posição 5000 da ram, temos algo assim:

1.  SOMA ← 0
2.  N ← 1
3.  POSICAO ← 5000
4.  SOMA ← SOMA + N
5.  N ← N + 1
6.  Comparar N com 10
7.  Se for menor ou igual, vá para passo 4
8.  Gravar em POSICAO o valor SOMA

Apesar de cada operação ser muito simples, o processador é capaz de executá-los muito rapidamente.
Ao juntar um monte de operações simples, o computador consegue realizar operações bastante complicadas.

A principal forma de medir a velocidade de um processador é pelo número de operações que eles faz por segundo, dada em Hertz:

-   1MHz - 1 milhão de operações por segundo
-   1GHz - 1 bilhão de operações por segundo



### Memória RAM

É onde se armazenam todas as informações que o computador está usando:

- Programa sendo executado
- Arquivos sendo lidos ou gravados
- Imagem que está sendo exibida na tela
- Milhões de coisas que são necessárias para o funcionamento interno dos programas.

Podemos imaginar uma memória como um monte de gavetas numeradas, cada uma capaz de armazenar uma pequena quantidade de informações.

![Memórias são como gavetas numeradas](http://qph.is.quoracdn.net/main-qimg-d155a726e4a15ec99d933de742fd7c2c)

Uma memória de computador possui um grande número de gavetas (por exemplo, 1 bilhão), sendo que cada gaveta (posição de memória) é capaz de armazenar uma quantidade pequena de dados (por exemplo, 1 byte / 8 bits). A capacidade total da memória é o numero de gavetas X tamanho da gaveta, neste caso 1 bilhão X 1 byte = 1 bilhão de bytes = 1GB.

Tamanhos tipicos:

-   Microcontroladores: 1KB - 512KB
-   Em roteadores Wifi: 16MB - 64MB
-   Telefones celulares: 256MB - 4GB
-   Desktops e Notebooks: 2GB - 16GB
-   Servidores de datacenter: 16GB - 256GB

![Módulo de memória](http://upload.wikimedia.org/wikipedia/commons/a/a0/PS2_RAM_Module.jpg)

### Dispositivo de armazenamento

TODO

Tamanhos tipicos:

-   Microcontroladores: 16KB - 512KB
-   Telefones e camera: 4GB - 64GB
-   Desktops e Notebooks: 250GB - 2TB

### Dispositivo de entrada e saída

Um computador só é útil se for capaz de interagir com o "mundo real", para isso ele se conecta a dispositivos de E/S.

É possível pendurar praticamente qualquer coisa em um computador, desde uma luz que pisca, até um carro que dirige sozinho.

Alguns tipos comuns de E/S:

-   Tela
-   Som
-   Mouse
-   Teclado
-   Tela de toque
-   Camera
-   Luzes
-   Sensores
-   Motores
-   Comunicação com outros computadores
