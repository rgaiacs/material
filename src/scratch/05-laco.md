---
track: Scratch
title: Laço
---

Até agora você teve que utilizar blocos para todas
as ações que os objetos executavam,
isso é, se você quer que seu programa execute por um minuto
você precisa adicionar blocos suficientes
para que ele execute por um minuto.
E se você desejar que seu programa execute por uma hora?
Supondo que você utilize blocos que duram um segundo
você precisaria de 3600 blocos.

Felizmente,
se seu programa possui uma ou mais ações que são repetidas
você pode economizar vários blocos utilizando
um ou mais laços,
isso é, um bloco cuja ação é repetir outros blocos.

No Scratch,
você pode criar laços utilizando os blocos

-   `sempre` ou
-   `repita ()`

presentes na palheta `Controle`.

![Screenshot da palheta `Controle` com destaque para laços.](/img/scratch/controle-lacos.jpg)

Com o bloco `sempre` você pode criar um programa
no qual um cachorro fica sempre correndo atrás de um gato.

Adicione o objeto `animals/dog1-a`.
Depois adicione os blocos na aba `Comandos` do `objeto2`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (150) y: (-100)
sempre
    aponte para a direção (-90)
    deslize em (1) segundos para x: (-150) y: (-100)
    aponte para a direção (0)
    deslize em (1) segundos para x: (-150) y: (100)
    aponte para a direção (90)
    deslize em (1) segundos para x: (150) y: (100)
    aponte para a direção (180)
    deslize em (1) segundos para x: (150) y: (-100)
    aponte para a direção (-90)
~~~

![Screenshot do `objeto2` com a aba `Comandos` preenchida.](/img/scratch/laco-cachorro.jpg)

Depois de programar o `objeto2`
você deve selecionar o `objeto1`
e programá-lo.

Adicione os blocos na aba `Comandos` do `objeto1`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (-150) y: (100)
sempre
    aponte para a direção (90)
    deslize en (1) segundos para x: (150) y: (100)
    aponte para a direção (180)
    deslize en (1) segundos para x: (150) y: (-100)
    aponte para a direção (-90)
    deslize en (1) segundos para x: (-150) y: (-100)
    aponte para a direção (0)
    deslize en (1) segundos para x: (-150) y: (100)
    aponte para a direção (90)
~~~

![Screenshot do `objeto1` com a aba `Comandos` preenchida.](/img/scratch/laco-gato.jpg)

<section class="exercises">
#### Exercício

Troque o bloco `sempre` pelo bloco `repita ()` no exemplo anterior.
</section>
