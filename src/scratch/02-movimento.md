---
track: Scratch
title: Movimento
---

No cando superior direito você encontra o palco
que corresponde ao lugar onde as ações são executadas.
O posicionamento dos objetos no palco
é feito utilizando valores para a posição horizontal (x) e vertical (y),
sendo que x tem que ser maior ou igual que -240 e menor ou igual que 240
e que y tem que ser maior ou igual que -180 e menor ou igual que 180.

![Screenshot destacando o palco](/img/scratch/eixos.jpg)

A posição x e y dos objetos presentes na figura anterior
é apresentado na tabela abaixo.

Objeto      Posição x Posição y
----------- --------- ---------
Cavalo      100       0
Dinossauro  0         100
Gato        -100      0
Elefante    0         -100

Se você selecionar a palheta de blocos `Movimentos`
você vai encontrar vários blocos correspondentes a movimentação
de um objeto.

Para escrever seu primeiro programa,
que consiste em mover o `objeto1`,
você precisa arrastar o bloco `deslize em () segundos para x: () y: ()`
para a aba `Comandos`
e trocar os valores de modo a ficar com

~~~
deslize em (5) segundos para x: (150) y: (0)
~~~

![Screenshot do bloco `deslize em (5) segundos para x: (150) y: (0)` dentro da aba comandos.](/img/scratch/deslize.jpg)

Para que seu programa seja executado
você precisa adicionar um bloco marcando o início do seu programa.
Você encontra esse bloco na palheta `Controle`.

![Screenshot do bloco `deslize em () segundos para x: () y: ()` e da palheta `Controle`.](/img/scratch/deslize-em-controle.jpg)

Na palheta `Controle`,
arraste o bloco `quando ⚑ clicado`
para a aba `Comandos`
e conecte-o com o bloco já existente nessa aba
de modo a ficar com

~~~
quando ⚑ clicado
deslize em (5) segundos para x: (150) y: (0)
~~~

![Screenshot do primeiro programa.](/img/scratch/primeiro-programa.jpg)

Com os dois blocos conectados
você acabou de criar seu primeiro programa em Scratch.
Para executar esse programa,
clique na bandeira verde no canto superior direito da tela.

<section class="exercises">
#### Exercício

Execute novamente seu programa. O que aconteceu?

Utilize o bloco `vá para x: () y: ()`
presente na palheta `Movimentos`
para corrigir o seu programa.
</section>
