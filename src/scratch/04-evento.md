---
track: Scratch
title: Evento
---

Quando você está trabalhando com mais de um objeto
é comum precisar sincronizar as ações de cada um deles.
Você pode fazer isso utilizando apenas blocos
que possuem uma medida de tempo associada.
Por exemplo,
você pode recriar um [quadrinho do Garfield](https://psychoartigo.files.wordpress.com/2013/05/ga920920.jpg)
utilizando dois objetos
sendo que os comandos do primeiro objeto são

~~~
quando ⚑ clicado
vá para x: (-100) y: (0)
aponte para a direção (90)
diga (Late!) por (2) segundos
espere (2) segundos
diga (Pula!) por (2) segundos
espere (2) segundos
diga (Morto!) por (2) segundos
~~~

e do segundo objeto são

~~~
quando ⚑ clicado
mude para o traje (dog1-a)
vá para x: (100) y: (0)
aponte para a direção (-90)
espere (2) segundos
diga (Au! Au!) por (2) segundos
espere (2) segundos
deslize em (1) segundo para x: (100) y: (50)
deslize em (1) segundo para x: (100) y: (0)
espere (2) segundos
mude para o traje (morto)
~~~

![Screenshot dos comandos do primeiro objeto quando utilizando tempo.](/img/scratch/dialogo-gato-tempo.jpg)

![Screenshot dos comandos do segundo objeto quando utilizando tempo.](/img/scratch/dialogo-cachorro-tempo.jpg)

Para programas simples o uso do tempo irá resolver seu problema
mas para programas mais complexos esse método irá fazer seu programa
funcionar de maneira inadequada,
por exemplo, quando você for esperar uma entrada do usuário.
A solução mais adequada é utilizar os blocos

-   `anuncie () para todos`,
-   `anuncie () para todos e espere`,
-   `quando eu ouvir ()`

presentes na palheta `Controle`.
Você pode reescrever o programa anterior utilizando esses blocos.
O primeiro objeto deverá ter os blocos

#.

    ~~~
    quando ⚑ clicado
    vá para x: (-100) y: (0)
    aponte para a direção (90)
    diga (Late!) por (2) segundos
    anuncie (late) para todos
    ~~~

#.

    ~~~
    quando eu ouvir (au)
    diga (Pula!) por (2) segundos
    anuncie (pula) para todos
    ~~~

#.

    ~~~
    quando eu ouvir (tap)
    diga (Morto!) por (2) segundos
    anuncie (morto) para todos
    ~~~

e o segundo objeto deverá ter os blocos

#.

    ~~~
    quando ⚑ clicado
    mude para o traje (dog1-a)
    vá para x: (100) y: (0)
    aponte para a direção (-90)
    ~~~

#.

    ~~~
    quando eu ouvir (late)
    diga (Au! Au!) por (2) segundos
    anuncie (au) para todos
    ~~~

#.

    ~~~
    quando eu ouvir (pula)
    deslize em (1) segundos para x: (100) y: (50)
    deslize em (1) segundos para x: (100) y: (50)
    deslize em (1) segundos para x: (100) y: (0)
    anuncie (tap) para todos
    ~~~
#.

    ~~~
    quando eu ouvir (morto)
    mude para o traje (morto)
    ~~~

![Screenshot dos comandos do primeiro objeto quando utilizando evento.](/img/scratch/dialogo-gato-evento.jpg)

![Screenshot dos comandos do segundo objeto quando utilizando evento.](/img/scratch/dialogo-cachorro-evento.jpg)

<aside>
#### Como a Disney/Pixar produz suas animações?

Disney/Pixar utiliza os mesmos princípios que você utilizou
para animar o quadrinho do Garfield.
A diferença é que a Disney/Pixar
utiliza softwares bem mais complexos que o Scratch,
se você tiver interesse teste o [Blender](https://www.blender.org/)
com o qual é [possível fazer animações profissionais](https://www.youtube.com/user/BlenderFoundation),
e computadores bem mais potentes que aqueles que você tem acesso.
</aside>

<section class="exercises">
#### Exercício

Troque os blocos `diga () por () segundos` por blocos `diga ()`
em ambos os programas. O que acontece em cada um dos casos?
</section>

<section class="exercises">
#### Exercício

Reescreva o seu programa que possui um diálogo, dessa vez utilizando eventos ao
invés de tempo.
</section>
