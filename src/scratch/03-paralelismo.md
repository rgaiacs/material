---
track: Scratch
title: Paralelismo
---

Até agora você apenas aninou um único objeto em seus programas.
Nessa seção você vai aprender como animar um segundo objeto.

O primeiro passo para poder animar um segundo objeto
é adicioná-lo no palco.
Você pode fazer isso utilizando

-   `pintar novo objeto`,
-   `escolher um sprinte do arquivo` ou
-   `pegar objeto surpresa`.

Essas opções encontram-se na região destacada abaixo.

![Screenshot destacando os botões para adicionar novo objeto.](/img/scratch/adiciona-objeto.jpg)

Neste exemplo você deve adicionar o objeto `animals/dog1-a`.

![Screenshot do `objecto2` adicionado.](/img/scratch/paralelismo-adiciona.jpg)

Adicione os blocos na aba `Comandos` do `objeto2`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (150) y: (-100)
aponte para a direção (-90)
deslize em (5) segundos para x: (-150) y: (-100)
~~~

![Screenshot do `objeto2` com a aba "Comandos" preenchida.](/img/scratch/paralelismo-cachorro.jpg)

Depois de programar o `objeto2`
você deve selecionar o `objeto1`, isso é, clicar no `objeto1`,
e programá-lo.

Adicione os blocos na aba `Comandos` do `objeto1`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (-150) y: (100)
aponte para a direção (90)
deslize en (5) segundos para x: (150) y: (100)
~~~

![Screenshot do `objeto1` com a aba "Comandos" preenchida.](/img/scratch/paralelismo-gato.jpg)

Agora que você programou os dois objetos
você pode executar seu programa
e ver ambos os objetos sendo animados simultaneamente.

![Screenshot do palco ao final da execução.](/img/scratch/paralelismo-bug.jpg)

Ao final da execução do seu programa,
o `objeto2` deve estar de ponta cabeça.
Você não deseja isso
e para corrigir esse bug
você deve utilizar a opção `somente esquerda-direita`
que encontram-se destacada na figura abaixo.

![Screenshot do palco ao final da execução.](/img/scratch/paralelismo-solucao.jpg)

<section class="exercises">
#### Exercício

Crie um programa no qual ocorre um diálogo entre dois objetos.
</section>
