---
track: Scratch
title: Interface
---

O Scratch é,
de acordo com a [Wikipédia](https://pt.wikipedia.org/wiki/Scratch),
"uma linguagem de programação criada em 2003 pelo Media Lab do MIT".
Com Scratch você pode programar animações e jogos.

Ao abrir o Scratch você vai se deparar com uma janela semelhante à
ilustrada na figura abaixo.

![Screenshot da tela inicial do Scratch.](/img/scratch/home.jpg)

A interface do Scratch é dividida,
resumidamente,
em quatro áreas:

-   palheta de blocos --- é onde você encontra os blocos que pode utilizar para
    compor seu programa;
-   palco --- é onde você visualiza seu programa sendo executado;
-   lista de objetos --- é onde você encontra os objetos presentes no seu
    programa **e adiciona novos** caso precise;
-   editor de objeto --- é onde você editar os objetos,
    isso é, adiciona ações, trajes, ...

Cada uma dessas quatro áreas encontram-se destacadas nas figuras abaixo.

![Screenshot da tela inicial do Scratch destacando a palheta de blocos.](/img/scratch/interface-palheta.jpg)

![Screenshot da tela inicial do Scratch destacando o palco.](/img/scratch/interface-palco.jpg)

![Screenshot da tela inicial do Scratch destacando a lista de objetos.](/img/scratch/interface-objetos.jpg)

![Screenshot da tela inicial do Scratch destacando o editor de objeto.](/img/scratch/interface-editor.jpg)

## Testando blocos

Você pode testar qualquer bloco presente na palheta de blocos
executando um clique duplo no bloco desejado.
Por exemplo,
selecione o palheta `Aparência`
e execute um clique duplo no bloco `diga ()`.

![Screenshot do Scratch ao testar o bloco `diga ()`.](/img/scratch/interface-teste.jpg)

#### Criando um palco

Palcos são imagens JPG ou PNG com 480x360 pixels.
Você pode criar novos palcos utilizando qualquer ferramenta
que crie um arquivo JPG ou PNG com 480x360 pixels
como por exemplo
o [Inkscape](https://inkscape.org/en/) ou
o [Gimp](http://www.gimp.org/).

![Palco criado no Inkscape. Os holofotes foram criados por grantm e disponibilizados em https://openclipart.org/detail/12227/student-spotlight sob domínio público.](/img/palco-jovemhacker.svg)

#### Criando um objeto

Objetos são imagens PNG com fundo transparente[^1].
Você pode criar novos objetos utilizando qualquer ferramenta
que crie um arquivo PNG com fundo transparente
como por exemplo
o [Inkscape](https://inkscape.org/en/) ou
o [Gimp](http://www.gimp.org/).

![Objeto criado no Inkscape.](/img/token-jovemhacker.svg)

[^1]:   Você precisa do fundo transparente caso contrário seu objeto será um
        retângulo. **Imagens JPG não suportam fundo transparente.**
