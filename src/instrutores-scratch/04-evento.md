---
track: Scratch
title: Evento
---

## Exemplos

-   Diálogo do Garfield
    -   [utilizando tempo](/instrutores-scratch/exemplos/conversa-garfield-tempo.sb)
    -   [utilizando evento](/instrutores-scratch/exemplos/conversa-garfield-evento.sb)
-   Diálogo de Star Wars (**extra**)
    -   [utilizando tempo](/instrutores-scratch/exemplos/conversa-starwars-tempo-en.sb)
    -   [utilizando evento](/instrutores-scratch/exemplos/conversa-starwars-evento-en.sb)

## Computação desplugada

Sem sugestão.

## Comentários

Nesse tema é apresentado uma melhor forma de sincronizar threads.

## Exercícios adicionais

Animar um diálogo famoso de Star Wars.

Os comandos do primeiro objeto são

~~~
quando ⚑ clicado
vá para x: <100> y: <0>
aponte para a direção <-90>
espere <2> segundos
espere <2> segundos
espere <2> segundos
diga <I'll never join you!> por <2> segundos
espere <2> segundos
diga <You killed him.> por <2> segundos
~~~

e do segundo objeto são

~~~
quando ⚑ clicado
vá para x: <-100> y: <0>
diga <There is no escape.> por <2> segundos
diga <Don't make me destroy you.> por <2> segundos
diga <Join me.> por <2> segundos
espere <2> segundos
diga <What happened to your father?> por <2> segundos
espere <2> segundos
diga <No. I am your father.> por <2> segundos
~~~

![Screenshot dos comandos do primeiro objeto quando utilizando tempo.](/img/scratch/dialogo-luke-tempo.jpg)

![Screenshot dos comandos do segundo objeto quando utilizando tempo.](/img/scratch/dialogo-vader-tempo.jpg)

Utilizando evento
o primeiro objeto deverá ter os blocos

1.

    ~~~
    quando ⚑ clicado
    vá para x: <100> y: <0>
    aponte para a direção <-90>
    ~~~

2.

    ~~~
    quando eu ouvir <join>
    diga <I'll never join you!> por <2> segundos
    anuncie <no> para todos
    ~~~

3.

    ~~~
    quando eu ouvir <father>
    diga <You killed him.> por <2> segundos
    anuncie <kill> para todos
    ~~~

e o segundo objeto deverá ter os blocos

1.

    ~~~
    quando ⚑ clicado
    vá para x: <-100> y: <0>
    diga <There is no escape.> por <2> segundos
    diga <Don't make me destroy you.> por <2> segundos
    diga <Join me.> por <2> segundos
    anuncie <join> para todos
    ~~~

2.

    ~~~
    quando eu ouvir <no>
    diga <What happened to your father?> por <2> segundos
    anuncie <father> para todos
    ~~~

3.

    ~~~
    quando eu ouvir <kill>
    diga <No. I am your father.> por <2> segundos
    ~~~

![Screenshot dos comandos do primeiro objeto quando utilizando evento.](/img/scratch/dialogo-luke-evento.jpg)

![Screenshot dos comandos do segundo objeto quando utilizando evento.](/img/scratch/dialogo-vader-evento.jpg)
